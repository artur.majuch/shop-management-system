﻿using MvcApplication.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace MvcApplication.Models
{
    /// <summary>
    /// Product class
    /// </summary>
    public class ProductModel
    {
        decimal price;
        int sundayBonus;
        int mondayBonus;
        int tuesdayBonus;
        int wednesdayBonus;
        int thursdayBonus;
        int fridayBonus;
        int saturdayBonus;

        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [MaxLength(50)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter name")]
        public string Name { get; set; }

        [MaxLength(50)]
        public string Description { get; set; }

        [Display(Name = "Best before")]
        [DataType(DataType.Date)]
        [DateRange("2016/01/01", "2016/12/12")]
        public DateTime BestBefore { get; set; }

        public byte[] Image { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string ImageType { get; set; }

        public virtual PriceItemModel PriceItem { get; set; }

        [NotMapped]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter price")]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "d")]
        public decimal Price
        {
            get
            {
                if (PriceItem != null)
                    return PriceItem.Price;
                else
                    return price;
            }
            set
            {
                PriceItem = null;
                price = value;
            }
        }

        [NotMapped]
        [Editable(false)]
        [Display(Name = "Price after bonus at now")]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "d")]
        public decimal PriceAfterBonus
        {
            get
            {
                if (PriceItem != null && PriceItem.Bonuses != null)
                {
                    int dayOfWeek = (int)DateTime.Now.Date.DayOfWeek;
                    List<BonusModel> items = new List<BonusModel>(PriceItem.Bonuses);
                    BonusModel item = items.Find(x => x.DayOfWeek == dayOfWeek);
                    if (item != null)
                    {
                        return PriceItem.Price - PriceItem.Price * (decimal)item.Percent / 100M;
                    }
                    else
                        return PriceItem.Price;
                }
                else
                    return price;
            }
        }

        [NotMapped]
        [Display(Name = "Bonus at Sunday[%]")]
        [Range(0,100, ErrorMessage = "Please enter value between 0 and 100")]
        public int SundayBonus
        {
            get
            {
                return GetBonusAtDay(sundayBonus, DayOfWeek.Sunday);
            }
            set
            {
                PriceItem = null;
                sundayBonus = value;
            }
        }

        [NotMapped]
        [Display(Name = "Bonus at Monday[%]")]
        [Range(0, 100, ErrorMessage = "Please enter value between 0 and 100")]
        public int MondayBonus
        {
            get
            {
                return GetBonusAtDay(mondayBonus, DayOfWeek.Monday);
            }
            set
            {
                PriceItem = null;
                mondayBonus = value;
            }
        }

        [NotMapped]
        [Display(Name = "Bonus at Tuesday[%]")]
        [Range(0, 100, ErrorMessage = "Please enter value between 0 and 100")]
        public int TuesdayBonus
        {
            get
            {
                return GetBonusAtDay(tuesdayBonus, DayOfWeek.Tuesday);
            }
            set
            {
                PriceItem = null;
                tuesdayBonus = value;
            }
        }

        [NotMapped]
        [Display(Name = "Bonus at Wednesday[%]")]
        [Range(0, 100, ErrorMessage = "Please enter value between 0 and 100")]
        public int WednesdayBonus
        {
            get
            {
                return GetBonusAtDay(wednesdayBonus, DayOfWeek.Wednesday);
            }
            set
            {
                PriceItem = null;
                wednesdayBonus = value;
            }
        }

        [NotMapped]
        [Display(Name = "Bonus at Thursday[%]")]
        [Range(0, 100, ErrorMessage = "Please enter value between 0 and 100")]
        public int ThursdayBonus
        {
            get
            {
                return GetBonusAtDay(thursdayBonus, DayOfWeek.Thursday);
            }
            set
            {
                PriceItem = null;
                thursdayBonus = value;
            }
        }

        [NotMapped]
        [Display(Name = "Bonus at Frirday[%]")]
        [Range(0, 100, ErrorMessage = "Please enter value between 0 and 100")]
        public int FridayBonus
        {
            get
            {
                return GetBonusAtDay(fridayBonus, DayOfWeek.Friday);
            }
            set
            {
                PriceItem = null;
                fridayBonus = value;
            }
        }

        [NotMapped]
        [Display(Name = "Bonus at Saturday[%]")]
        [Range(0, 100, ErrorMessage = "Please enter value between 0 and 100")]
        public int SaturdayBonus
        {
            get
            {
                return GetBonusAtDay(saturdayBonus, DayOfWeek.Saturday);
            }
            set
            {
                PriceItem = null;
                saturdayBonus = value;
            }
        }

        /// <summary>
        /// get current day of week
        /// </summary>
        [NotMapped]
        public int CurrentDayOfWeek
        {
            get
            {
                return (int)DateTime.Now.Date.DayOfWeek;
            }
        }

        public ProductModel()
        {
            Id = 0;
            BestBefore = DateTime.Now.Date;
        }

        private int GetBonusAtDay(int bonusAtDay, DayOfWeek dayOfWeek)
        {
            if (PriceItem != null && PriceItem.Bonuses != null)
            {
                List<BonusModel> items = new List<BonusModel>(PriceItem.Bonuses);
                BonusModel item = items.Find(x => x.DayOfWeek == (int)dayOfWeek);
                if (item != null)
                {
                    return item.Percent;
                }
                else
                    return bonusAtDay;
            }
            else
            {
                return bonusAtDay;
            }
        }
    }
}