﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MvcApplication.Models
{
    public class BonusModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter percent")]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "d")]
        public int Percent { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter day of week")]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "i")]
        public int DayOfWeek { get; set; }

        public int PriceId { get; set; }
       
        [Required]
        [JsonIgnore]
        [ScriptIgnore]
        public virtual PriceItemModel PriceItem { get; set; }

        public BonusModel()
        {
            Id = 0;
        }
    }
}