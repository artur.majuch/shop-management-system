﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MvcApplication.Models
{
    public class PriceItemModel
    {
        [HiddenInput(DisplayValue = false)]
        public int ProductId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter price")]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "d")]
        public decimal Price { get; set; }
                
        [Required]
        public virtual ProductModel Product { get; set; }

        public virtual ICollection<BonusModel> Bonuses { get; set; }

        public PriceItemModel()
        {
        }
    }
}