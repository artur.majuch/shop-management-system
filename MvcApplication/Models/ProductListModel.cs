﻿using MvcApplication.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace MvcApplication.Models
{
    /// <summary>
    /// Product model for list
    /// </summary>
    public class ProductListModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime BestBefore { get; set; }

        public decimal Price { get; set; }
        
        [NotMapped]
        public decimal PriceAfterBonus
        {
            get
            {
                return Price - Price * (decimal)CurrentBonus / 100M;
            }
        }

        public int CurrentBonus { get; set; }

        public bool HasImage { get; set; }
    }
}