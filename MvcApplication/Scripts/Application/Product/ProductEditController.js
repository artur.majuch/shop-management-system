﻿function ProductEditController() {
    var self = this;

    self.calcPriceAfterBonus = function (e) {
        e.preventDefault();
        var currentBonus;
        var price = $('#Price').val();

        switch (self.currentDayOfWeek) {
            case 0:
                currentBonus = $('#SundayBonus').val();
                break;
            case 1:
                currentBonus = $('#MondayBonus').val();
                break;
            case 2:
                currentBonus = $('#TuesdayBonus').val();
                break;
            case 3:
                currentBonus = $('#WednesdayBonus').val();
                break;
            case 4:
                currentBonus = $('#ThursdayBonus').val();
                break;
            case 5:
                currentBonus = $('#FridayBonus').val();
                break;
            case 6:
                currentBonus = $('#SundayBonus').val();
                break;
            default:
                currentBonus = 0;

        }
        var priceAfterBonus = '???';
        if ($.isNumeric(price) && $.isNumeric(currentBonus)) {
            priceAfterBonus = price - price * currentBonus / 100;
        }
        $('#PriceAfterBonus').val(priceAfterBonus);
    }
   

    self.init = function (currentDayOfWeek) {
        self.currentDayOfWeek = currentDayOfWeek;
        $('input[data-type="type-bonus"]').keyup(self.calcPriceAfterBonus);
    };
}