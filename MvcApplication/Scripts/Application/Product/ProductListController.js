﻿function ProductListController() {
    var self = this;

    self.getProductRow = function (e) {
        var $button = $(e.target);
        var $productRow = $button.parents("tr:first");
        return $productRow;
    }

    self.getProductId = function (e) {
        var $productRow = self.getProductRow(e);
        var productId = $productRow.attr("data-product-id");
        return productId;
    }

    self.getProductName = function (e) {
        var $productRow = self.getProductRow(e);
        var productName = $productRow.attr("data-product-name");
        return productName;
    }

    /// show product image in separate window
    self.showImage = function (e) {
        e.preventDefault();
        var productId = self.getProductId(e);

        dlg = $("#dialog-img");
        dlg.dialog({
            autoOpen: false, height: 600, width: 800, modal: true, open: function () {
                $("#my-img").attr('src', '/Product/GetImage?prodId=' + productId);
            }, buttons: {
                "Close": function () {
                    $(this).effect('explode', null, 2000);
                    $(this).dialog('close');
                }
            }
        });
        dlg.dialog("open");
    }

    self.delete = function (e) {
        e.preventDefault();
        var productId = self.getProductId(e);

        $("#dialog").dialog({
            autoOpen: false,
            modal: true,
            open: function() {
                $('#label-message').text('Do you realy want to delete product: ' + self.getProductName(e));
            },
            buttons: {
                "Confirm": function () {
                    var product = {
                        Id: productId,
                    }

                    $.ajax({
                        url: '/product/delete',
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            if (data.resultCode == 0) {
                                $(e.target).parents("tr:first").remove();
                            }
                            $("#dialog").dialog("close");
                            alert(data.message);
                        },
                        data: product
                    });
                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });
        $("#dialog").dialog("open");
    }

    self.init = function () {
        $(document).on('click', 'button.type-show-image', self.showImage);
        $(document).on('click', 'button.type-delete', self.delete);
    };
}