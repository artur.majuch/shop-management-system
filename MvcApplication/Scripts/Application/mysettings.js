﻿$(document).ready(function () {
    $.culture = Globalize.culture("en-GB");
    $.validator.methods.date = function (value, element) {

        //This is not ideal but Chrome passes dates through in ISO1901 format regardless of locale 
        //and despite displaying in the specified format.
        return this.optional(element)
            || Globalize.parseDate(value, "dd/mm/yyyy", "en-GB")
            || Globalize.parseDate(value, "dd/mm/yyyy");
    }

    
    $('table tr:not(.Header):even').addClass('d0');
    $('table tr:not(.Header):odd').addClass("d1");
});

$(function () {
    $("img").draggable();
});

$(function () {
    $('#BestBefore').datepicker({ dateFormat: "dd/mm/yy" });
});

jQuery(function ($) {
    /*
    $.datepicker.regional['en'] = {
        closeText: 'Zamknij', prevText: '&#x3c;Poprzedni', nextText: 'Następny&#x3e;', currentText: 'Dziś', monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'], monthNamesShort: ['Sty', 'Lu', 'Mar', 'Kw', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Pa', 'Lis', 'Gru'], dayNames: ['Niedziela', 'Poniedzialek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
        dayNamesShort: ['Nie', 'Pn', 'Wt', 'Śr', 'Czw', 'Pt', 'So'], dayNamesMin: ['N', 'Pn', 'Wt', 'Śr', 'Cz', 'Pt', 'So'], weekHeader: 'Tydz', dateFormat: 'yy-mm-dd', firstDay: 1, isRTL: false, showMonthAfterYear: false, yearSuffix: ''
    };
    */
    $.datepicker.setDefaults($.datepicker.regional['en']);
});
