﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication.Models;
using MvcApplication.Infrastructure.Interfaces;
using MvcApplication.Infrastructure;
using MvcApplication.Attributes;

namespace MvcApplication.Controllers
{
    /// <summary>
    /// Product controller - allowed for user in admin role
    /// </summary>
    [Authorize(Roles ="admin")]
    public class ProductController : BaseController
    {
        IProductRepository rep;

        public ProductController(IProductRepository r)
        {
            rep = r;
        }

        public ActionResult List()
        {
            return View(rep.Products());
        }

        public ActionResult Edit(int id)
        {
            ProductModel prod = rep.GetProduct(id);
            return View(prod);
        }

        [HttpPost]
        public ActionResult Edit(ProductModel prod, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    prod.ImageType = file.ContentType;
                    prod.Image = new byte[file.ContentLength];
                    file.InputStream.Read(prod.Image, 0, file.ContentLength);
                }
                rep.SaveProduct(prod);
                TempData["message"] = string.Format("\"{0}\" - successfuly saved", prod.Name);
                return RedirectToAction("List");
            }
            return View("Edit",prod);
        }

        [HttpPost]
        public ActionResult Delete(ProductModel prod)
        {
            rep.DeleteProduct(prod.Id);
            return new JsonResult()
            {
                Data = new JsonRequestResult()
                {
                    message = "Product deleted",
                    resultCode = 0
                },

                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult Create()
        {
            return View("Edit", new ProductModel());
        }

        [HttpGet]
        public FileContentResult GetImage(int prodId)
        {
            ProductModel prod = rep.GetProduct(prodId);
            if (prod != null && prod.Image!=null)
                return File(prod.Image, prod.ImageType);
            else
                return null;
        }
    }
}