﻿using MvcApplication.Models;
using System.Collections.Generic;

namespace MvcApplication.Infrastructure.Interfaces
{
    /// <summary>
    /// Product repository
    /// </summary>
    public interface IProductRepository
    {
        IList<ProductListModel> Products();
        void SaveProduct(ProductModel prod);
        void DeleteProduct(int id);
        ProductModel GetProduct(int id);
    }
}