﻿using MvcApplication.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using MvcApplication.Models;
using System.Data.SqlClient;
using System.Data;

namespace MvcApplication.Infrastructure.ADO
{
    public class AdoProductRepository : IProductRepository
    {
        public void DeleteProduct(int id)
        {
            SqlConnection connection;
            SqlCommand command;
            InitQuery(out connection, out command);
            command.CommandText = "dbo.DeleteProduct";
            command.Parameters.Add(new SqlParameter("@ProductId", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = id;

            int result = command.ExecuteNonQuery();

            //we can use to identify operation result
            int resultCode = (int)command.Parameters[0].Value;

            connection.Close();
        }

        public ProductModel GetProduct(int id)
        {
            SqlConnection connection;
            SqlCommand command;
            InitQuery(out connection, out command);
            command.CommandText = "dbo.GetProduct";
            command.Parameters.Add(new SqlParameter("@ProductId", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = id;

            SqlDataReader reader = command.ExecuteReader();

            ProductModel prod = null;

            if (reader.HasRows)
            {
                prod = new ProductModel();
                if (reader.Read())
                {
                    ReadProduct(reader, prod);
                }
                reader.NextResult();

                prod.PriceItem.Bonuses = new List<BonusModel>();

                while (reader.Read())
                {
                    BonusModel bonus = new BonusModel();
                    bonus.Id = reader.GetInt32(0);
                    bonus.Percent = reader.GetInt32(1);
                    bonus.DayOfWeek = reader.GetInt32(2);
                    bonus.PriceId = reader.GetInt32(3);
                    prod.PriceItem.Bonuses.Add(bonus);
                }

            }
            reader.Close();

            //we can use to identify operation result
            int resultCode = (int)command.Parameters[0].Value;

            connection.Close();
            return prod;
        }

        public IList<ProductListModel> Products()
        {
            SqlConnection connection;
            SqlCommand command;
            InitQuery(out connection, out command);
            command.CommandText = "dbo.GetAllProducts";

            SqlDataReader reader = command.ExecuteReader();

            IList<ProductListModel> result = new List<ProductListModel>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ProductListModel prod = new ProductListModel();

                    prod.Id = reader.GetInt32(0);
                    prod.Name = reader.GetString(1);
                    if (!reader.IsDBNull(2))
                        prod.Description = reader.GetString(2);
                    if (!reader.IsDBNull(3))
                        prod.BestBefore = reader.GetDateTime(3);
                    prod.Price = reader.GetDecimal(4);
                    prod.HasImage = reader.GetBoolean(5);
                    prod.CurrentBonus = reader.GetInt32(6);

                    result.Add(prod);
                }
            }
            reader.Close();

            //we can use to identify operation result
            int resultCode = (int)command.Parameters[0].Value;

            connection.Close();
            return result;
        }

        public void SaveProduct(ProductModel prod)
        {
            SqlConnection connection;
            SqlCommand command;
            InitQuery(out connection, out command);
            command.CommandText = "dbo.SaveProduct";
            command.Parameters.Add(new SqlParameter("@ProductId", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = prod.Id;
            command.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar, prod.Name.Length, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = prod.Name;

            if (prod.Description != null)
            {
                command.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar, prod.Description.Length, ParameterDirection.Input, 10, 0, null,
                    DataRowVersion.Current, false, null, "", "", "")).Value = prod.Description;
            }
            command.Parameters.Add(new SqlParameter("@Price", SqlDbType.Decimal, 18, ParameterDirection.Input, 18, 2, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = prod.Price;

            if (prod.BestBefore != null)
            {
                command.Parameters.Add(new SqlParameter("@BestBefore", SqlDbType.DateTime, 8, ParameterDirection.Input, 10, 0, null,
                    DataRowVersion.Current, false, null, "", "", "")).Value = prod.BestBefore;
            }

            if (prod.Image != null)
            {
                command.Parameters.Add(new SqlParameter("@Image", SqlDbType.Image, prod.Image.Length, ParameterDirection.Input, 10, 0, null,
                    DataRowVersion.Current, false, null, "", "", "")).Value = prod.Image;
                command.Parameters.Add(new SqlParameter("@ImageType", SqlDbType.NVarChar, prod.ImageType.Length, ParameterDirection.Input, 10, 0, null,
                    DataRowVersion.Current, false, null, "", "", "")).Value = prod.ImageType;
            }

            command.Parameters.Add(new SqlParameter("@SundayBonus", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = prod.SundayBonus;
            command.Parameters.Add(new SqlParameter("@MondayBonus", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = prod.MondayBonus;
            command.Parameters.Add(new SqlParameter("@TuesdayBonus", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = prod.TuesdayBonus;
            command.Parameters.Add(new SqlParameter("@WednesdayBonus", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = prod.WednesdayBonus;
            command.Parameters.Add(new SqlParameter("@ThursdayBonus", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = prod.ThursdayBonus;
            command.Parameters.Add(new SqlParameter("@FridayBonus", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = prod.FridayBonus;
            command.Parameters.Add(new SqlParameter("@SaturdayBonus", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", "")).Value = prod.SaturdayBonus;

           
            command.Parameters.Add(new SqlParameter("@OutProductId", SqlDbType.Int, 4, ParameterDirection.Output, 10, 0, null,
                DataRowVersion.Current, false, null, "", "", ""));

            int result = command.ExecuteNonQuery();

            prod.Id = (int)command.Parameters["@OutProductId"].Value;

            //we can use to identify operation result
            int resultCode = (int)command.Parameters[0].Value;

            connection.Close();
        }

        private static void InitQuery(out SqlConnection connection, out SqlCommand command)
        {
            connection = new SqlConnection();
            connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString;
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Variant, 0, ParameterDirection.ReturnValue, 0, 0,
                null, DataRowVersion.Current, false, null, "", "", ""));

            connection.Open();
        }

        private static void ReadProduct(SqlDataReader reader, ProductModel prod)
        {
            prod.Id = reader.GetInt32(0);
            prod.Name = reader.GetString(1);
            if (!reader.IsDBNull(2))
                prod.Description = reader.GetString(2);
            if (!reader.IsDBNull(3))
                prod.BestBefore = reader.GetDateTime(3);
            if (!reader.IsDBNull(4))
                prod.Image = reader.GetSqlBytes(4).Buffer;
            if (!reader.IsDBNull(5))
                prod.ImageType = reader.GetString(5);
            prod.PriceItem = new PriceItemModel();
            prod.PriceItem.Price = reader.GetDecimal(6);
        }
    }
}