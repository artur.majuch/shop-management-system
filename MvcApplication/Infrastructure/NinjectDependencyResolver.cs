﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MvcApplication.Models;
using Ninject;
using MvcApplication.Infrastructure.Interfaces;
using MMvcApplication.Infrastructure.EntityFramework;
using MvcApplication.Infrastructure.ADO;

namespace MvcApplication.Infrastructure
{
    /// <summary>
    /// Dependency resolver
    /// </summary>
    public class NinjectDependencyResolver: IDependencyResolver
    {
        IKernel kernel;

        public NinjectDependencyResolver()
        {
            kernel = new StandardKernel();
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            string selectedDal = System.Configuration.ConfigurationManager.AppSettings["CustomDal"].ToString();
            switch (selectedDal)
            {
                case "EF":
                    kernel.Bind<IProductRepository>().To<EFProductRepository>();
                    break;
                case "ADO":
                    kernel.Bind<IProductRepository>().To<AdoProductRepository>();
                    break;
            }
        }
    }
}