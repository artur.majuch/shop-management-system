﻿using MvcApplication.Infrastructure.EntityFramework;
using MvcApplication.Infrastructure.Interfaces;
using MvcApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;


namespace MMvcApplication.Infrastructure.EntityFramework
{
    public class EFProductRepository: IProductRepository
    {
        /// <summary>
        /// Get all products
        /// </summary>
        /// <returns></returns>
        public IList<ProductListModel> Products()
        {
            EFDbContext context = new EFDbContext();
            using (var transaction = context.Database.BeginTransaction())
            {
                return context.Database.SqlQuery<ProductListModel>("dbo.GetAllProducts").ToList();
                //return context.Products.ToList();
            }
        }

        /// <summary>
        /// Save new or existing product
        /// </summary>
        /// <param name="prod"></param>
        public void SaveProduct(ProductModel prod)
        {
            EFDbContext context = new EFDbContext();

            using (var transaction = context.Database.BeginTransaction())
            {
                if (prod.Id == 0)
                {
                    decimal price = prod.Price;
                    ProductModel p = context.Products.Add(prod);
                    p.PriceItem = new PriceItemModel();
                    p.PriceItem.Price = price;
                    p.PriceItem.Bonuses = new List<BonusModel>();

                    UpdateBonuses(context, p, prod);
                }
                else
                {
                    ProductModel entry = context.Products.FirstOrDefault(p => p.Id == prod.Id);
                    entry.Description = prod.Description;
                    entry.Name = prod.Name;
                    entry.BestBefore = prod.BestBefore;
                    entry.PriceItem.Price = prod.Price;

                    UpdateBonuses(context, entry, prod);

                    if (prod.Image != null)
                    {
                        entry.Image = prod.Image;
                        entry.ImageType = prod.ImageType;
                    }
                }
                context.SaveChanges();
                transaction.Commit();
            }
        }

        /// <summary>
        /// Delete product and all related entities
        /// </summary>
        /// <param name="prodId"></param>
        public void DeleteProduct(int prodId)
        {
            EFDbContext context = new EFDbContext();
            using (var transaction = context.Database.BeginTransaction())
            {
                ProductModel entry = context.Products.FirstOrDefault(p => p.Id == prodId);
                if (entry != null)
                {
                    context.Products.Remove(entry);
                    context.SaveChanges();
                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Get product
        /// </summary>
        /// <param name="prodId"></param>
        /// <returns></returns>
        public ProductModel GetProduct(int prodId)
        {
            EFDbContext context = new EFDbContext();
            using (var transaction = context.Database.BeginTransaction())
            {
                ProductModel entry = context.Products.FirstOrDefault(p => p.Id == prodId);
                return entry;
            }
        }

        private static void UpdateBonuses(EFDbContext dbCtx, ProductModel prod, ProductModel prodBonus)
        {
            UpdateBonus(dbCtx, prod, prodBonus.SundayBonus, DayOfWeek.Sunday);
            UpdateBonus(dbCtx, prod, prodBonus.MondayBonus, DayOfWeek.Monday);
            UpdateBonus(dbCtx, prod, prodBonus.TuesdayBonus, DayOfWeek.Tuesday);
            UpdateBonus(dbCtx, prod, prodBonus.WednesdayBonus, DayOfWeek.Wednesday);
            UpdateBonus(dbCtx, prod, prodBonus.ThursdayBonus, DayOfWeek.Thursday);
            UpdateBonus(dbCtx, prod, prodBonus.FridayBonus, DayOfWeek.Friday);
            UpdateBonus(dbCtx, prod, prodBonus.SaturdayBonus, DayOfWeek.Saturday);
        }

        private static void UpdateBonus(EFDbContext dbCtx, ProductModel prod, int bonusAtDay, DayOfWeek dayOfWeek)
        {
            if (prod.Id > 0)
            {
                // Get fresh data from database
                var existingBonuses = dbCtx.Bonuses.Where(s => s.PriceId == prod.Id);
                if (bonusAtDay > 0)
                {
                    BonusModel existingBonus = existingBonuses.Where(s => s.DayOfWeek == (int)dayOfWeek).FirstOrDefault();
                    if (existingBonus != null)
                    {
                        existingBonus.Percent = bonusAtDay;
                    }
                    else
                    {
                        BonusModel newBonus = new BonusModel();
                        newBonus.DayOfWeek = (int)dayOfWeek;
                        newBonus.Percent = bonusAtDay;
                        newBonus.PriceId = prod.Id;
                        dbCtx.Bonuses.Add(newBonus);
                    }
                }
                else
                {
                    BonusModel existingBonus = existingBonuses.Where(s => s.DayOfWeek == (int)dayOfWeek).FirstOrDefault();
                    if (existingBonus != null)
                    {
                        dbCtx.Bonuses.Remove(existingBonus);
                    }
                }
            }
            else
            {
                // add new bonus
                if (bonusAtDay > 0)
                {
                    BonusModel bonus = new BonusModel();
                    bonus.DayOfWeek = (int)dayOfWeek;
                    bonus.Percent = bonusAtDay;
                    prod.PriceItem.Bonuses.Add(bonus);
                }
            }
        }
    }
}