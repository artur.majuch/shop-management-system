﻿using MvcApplication.Models;
using System.Data.Entity;

namespace MvcApplication.Infrastructure.EntityFramework
{
    public class EFDbContext : DbContext
    {
        public DbSet<ProductModel> Products { get; set; }
        public DbSet<PriceItemModel> PriceItems { get; set; }
        public DbSet<BonusModel> Bonuses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BonusModel>().Property(a => a.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<ProductModel>().Property(a => a.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            
            modelBuilder.Entity<BonusModel>().ToTable("Bonuses");
            modelBuilder.Entity<PriceItemModel>().ToTable("PriceItems");
            modelBuilder.Entity<ProductModel>().ToTable("Products");

            modelBuilder.Entity<PriceItemModel>().HasKey(e => e.ProductId);

            modelBuilder.Entity<ProductModel>()
                .HasRequired(s => s.PriceItem)
                .WithRequiredPrincipal(p => p.Product);
           
            modelBuilder.Entity<PriceItemModel>()
                .HasMany(s => s.Bonuses)
                .WithRequired(p => p.PriceItem)
                .HasForeignKey(s => s.PriceId);

            base.OnModelCreating(modelBuilder);
        }
    }
}