﻿CREATE TABLE [dbo].[Customers]
(
	[Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR(50)    NULL, 
    [Surname]     NVARCHAR(50)    NULL, 
    [Email]       NVARCHAR(50)    NULL, 
    [LoginId]     INT             NOT NULL,
	CONSTRAINT [PK_Customers] PRIMARY KEY ([Id])
)
