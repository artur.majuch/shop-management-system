﻿CREATE TABLE [dbo].[PriceItems] (
    [Price]     DECIMAL (18, 2) NOT NULL,
    [ProductId] INT             NOT NULL,
    CONSTRAINT [PK_PriceItems] PRIMARY KEY CLUSTERED ([ProductId] ASC),
    CONSTRAINT [FK_PriceItems_Products] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Products] ([Id]) ON DELETE CASCADE
);
