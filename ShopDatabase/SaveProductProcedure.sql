﻿CREATE PROCEDURE [dbo].[SaveProduct]
	@productId int,
	@name nvarchar(50),
	@description nvarchar(50) = null,
	@price decimal(18,2),
	@bestBefore datetime = null,
	@image image = null,
	@imageType nvarchar(20) = null,
	@sundayBonus int,
    @mondayBonus int,
    @tuesdayBonus int,
    @wednesdayBonus int,
    @thursdayBonus int,
    @fridayBonus int,
    @saturdayBonus int,
	@outProductId int OUTPUT
AS
	BEGIN TRY
		BEGIN TRANSACTION
			IF @productId = 0
				BEGIN
					INSERT INTO Products (Name, [Description], BestBefore, [Image], ImageType)
					VALUES (@name, @description, @bestBefore, @image, @imageType)

					SET @outProductId = SCOPE_IDENTITY()

					INSERT INTO PriceItems (Price, ProductId)
					VALUES (@price, @outProductId)

					GOTO UPDATE_BONUSES

				END
			ELSE
				BEGIN
					IF @image IS NOT NULL
						BEGIN
							UPDATE Products
							SET Name = @name, Description = @description, BestBefore = @bestBefore, [Image] = @image, ImageType = @imageType
							WHERE Products.Id = @productId
						END
					ELSE
						BEGIN
							UPDATE Products
							SET Name = @name, Description = @description, BestBefore = @bestBefore
							WHERE Products.Id = @productId
						END

					UPDATE PriceItems
					SET Price = @price
					WHERE PriceItems.ProductId = @productId

					--TODO: optimization is possible
					DELETE FROM Bonuses WHERE Bonuses.PriceId = @productId

					SET @outProductId = @productId
					GOTO UPDATE_BONUSES
				END
UPDATE_BONUSES:
		IF @sundayBonus > 0
			BEGIN
				INSERT INTO Bonuses (PriceId, [Percent], [DayOfWeek])
				VALUES (@outProductId, @sundayBonus, 0)
			END
		IF @mondayBonus > 0
			BEGIN
				INSERT INTO Bonuses (PriceId, [Percent], [DayOfWeek])
				VALUES (@outProductId, @mondayBonus, 1)
			END
		IF @tuesdayBonus > 0
			BEGIN
				INSERT INTO Bonuses (PriceId, [Percent], [DayOfWeek])
				VALUES (@outProductId, @tuesdayBonus, 2)
			END
		IF @wednesdayBonus > 0
			BEGIN
				INSERT INTO Bonuses (PriceId, [Percent], [DayOfWeek])
				VALUES (@outProductId, @wednesdayBonus, 3)
			END
		IF @thursdayBonus > 0
			BEGIN
				INSERT INTO Bonuses (PriceId, [Percent], [DayOfWeek])
				VALUES (@outProductId, @thursdayBonus, 4)
			END
		IF @fridayBonus > 0
			BEGIN
				INSERT INTO Bonuses (PriceId, [Percent], [DayOfWeek])
				VALUES (@outProductId, @fridayBonus, 5)
			END
		IF @saturdayBonus > 0
			BEGIN
				INSERT INTO Bonuses (PriceId, [Percent], [DayOfWeek])
				VALUES (@outProductId, @saturdayBonus, 6)
			END

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		RETURN -1
	END CATCH

RETURN 0
