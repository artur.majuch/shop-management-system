﻿CREATE PROCEDURE [dbo].[GetProduct]
	@productId int
AS
	BEGIN TRY
		BEGIN TRANSACTION
			SELECT Products.Id, Products.Name, Products.[Description], Products.BestBefore, Products.[Image], Products.ImageType, PriceItems.Price
			FROM Products
			INNER JOIN PriceItems ON Products.Id = PriceItems.ProductId
			WHERE (Products.Id = @productId)

			SELECT Bonuses.Id,  Bonuses.[Percent], Bonuses.DayOfWeek, Bonuses.PriceId
			FROM Bonuses
			WHERE (Bonuses.PriceId = @productId)
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		RETURN -1
	END CATCH

RETURN 0
