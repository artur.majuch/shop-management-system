﻿CREATE TABLE [dbo].[Bonuses]
(
	[Id]          INT             IDENTITY (1, 1) NOT NULL,
	[PriceId]     INT NOT NULL, 
    [Percent]     INT NOT NULL, 
    [DayOfWeek]   INT NOT NULL, 
	CONSTRAINT [FK_Bonuses_PriceItems] FOREIGN KEY ([PriceId]) REFERENCES [PriceItems]([ProductId]) ON DELETE CASCADE, 
	CONSTRAINT [PK_Bonuses] PRIMARY KEY ([Id])
);

GO

CREATE UNIQUE INDEX IDX_PriceId_DayOfWeek ON [dbo].[Bonuses] ([PriceId] ASC, [DayOfWeek] ASC); 
