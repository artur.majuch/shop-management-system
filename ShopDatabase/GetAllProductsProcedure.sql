﻿CREATE PROCEDURE [dbo].[GetAllProducts]
AS
	BEGIN TRY
		
		BEGIN TRANSACTION
			SELECT Products.Id, Products.Name, Products.[Description], Products.BestBefore, PriceItems.Price,
				CAST(CASE WHEN (Products.ImageType IS NULL) THEN 0 ELSE 1 END AS BIT)
				AS HasImage,
				ISNULL((SELECT [Percent] FROM Bonuses WHERE Bonuses.PriceId = Products.Id AND Bonuses.DayOfWeek = (DATEPART(dw,GETDATE()) - 1)), 0)
				AS CurrentBonus
			FROM Products
			INNER JOIN PriceItems ON Products.Id = PriceItems.ProductId
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		RETURN -1
	END CATCH
RETURN 0