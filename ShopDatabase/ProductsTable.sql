﻿CREATE TABLE [dbo].[Products] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)   NOT NULL,
    [Description] NVARCHAR (50)   NULL,
    [BestBefore]  DATETIME        NULL,
    [Image]       VARBINARY (MAX) NULL,
    [ImageType]   NVARCHAR(20)  NULL, 
    CONSTRAINT [PK_Products] PRIMARY KEY ([Id])
);

