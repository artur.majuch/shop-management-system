﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcApplication.Controllers;
using MvcApplication.Models;
using Moq;
using MvcApplication.Infrastructure.EntityFramework;
using MvcApplication.Infrastructure.Interfaces;
using MvcApplication.Infrastructure.ADO;
using MMvcApplication.Infrastructure.EntityFramework;

namespace MvcApplication.Tests.Controllers
{
    [TestClass]
    public class ProductControllerTest
    {
        [TestMethod]
        public void ProductsAddEF()
        {
            EFDbContext db = new EFDbContext();
            ProductModel prod = new ProductModel();
            prod.Description = "Description13";
            prod.Name = "Name13";
            ProductModel p = db.Products.Add(prod);
            p.PriceItem = new PriceItemModel();
            p.PriceItem.Price = 2.99M;
            BonusModel bonus = new BonusModel();
            bonus.DayOfWeek = 2;
            bonus.Percent = 30;
            p.PriceItem.Bonuses = new List<BonusModel>();
            p.PriceItem.Bonuses.Add(bonus);
            db.SaveChanges();

            ProductModel prodResult = db.Products.Where(x => x.Id == p.Id).First();
            
            Assert.AreEqual(prodResult.PriceItem.Price, 2.99M);

            db.Products.Remove(prodResult);
            db.SaveChanges();

            int count = db.Products.Where(x => x.Id == prodResult.Id).Count<ProductModel>();

            Assert.AreEqual(count, 0);
        }

        /// <summary>
        /// Main test for repository logic
        /// </summary>
        [TestMethod]
        public void ProductsMainTest()
        {
            ProductTests(new EFProductRepository());
            ProductTests(new AdoProductRepository());
        }

        private static void ProductTests(IProductRepository rep)
        {
            ProductModel prod = new ProductModel();
            prod.Description = "Description13";
            prod.Name = "Name13";
            prod.Price = 2.99M;

            prod.SundayBonus = 0;
            prod.MondayBonus = 1;
            prod.TuesdayBonus = 2;
            prod.WednesdayBonus = 3;
            prod.ThursdayBonus = 4;
            prod.FridayBonus = 5;
            prod.SaturdayBonus = 6;

            rep.SaveProduct(prod);
            int prodId = prod.Id;

            ProductModel existingProd = rep.GetProduct(prodId);

            Assert.AreEqual(existingProd.Name, "Name13");
            Assert.AreEqual(existingProd.Price, 2.99M);

            Assert.AreEqual(existingProd.SundayBonus, 0);
            Assert.AreEqual(existingProd.MondayBonus, 1);
            Assert.AreEqual(existingProd.TuesdayBonus, 2);
            Assert.AreEqual(existingProd.WednesdayBonus, 3);
            Assert.AreEqual(existingProd.ThursdayBonus, 4);
            Assert.AreEqual(existingProd.FridayBonus, 5);
            Assert.AreEqual(existingProd.SaturdayBonus, 6);

            existingProd.SundayBonus = 10;
            existingProd.MondayBonus = 11;
            existingProd.TuesdayBonus = 12;
            existingProd.WednesdayBonus = 13;
            existingProd.ThursdayBonus = 14;
            existingProd.FridayBonus = 15;
            existingProd.SaturdayBonus = 16;

            rep.SaveProduct(existingProd);

            existingProd = rep.GetProduct(prodId);

            Assert.AreEqual(existingProd.SundayBonus, 10);
            Assert.AreEqual(existingProd.MondayBonus, 11);
            Assert.AreEqual(existingProd.TuesdayBonus, 12);
            Assert.AreEqual(existingProd.WednesdayBonus, 13);
            Assert.AreEqual(existingProd.ThursdayBonus, 14);
            Assert.AreEqual(existingProd.FridayBonus, 15);
            Assert.AreEqual(existingProd.SaturdayBonus, 16);

            rep.DeleteProduct(prodId);

            existingProd = rep.GetProduct(prodId);

            Assert.IsNull(existingProd);
        }

        [TestMethod]
        public void TestMock()
        {
            /*
            Mock<IBonus> mock = new Mock<IBonus>();
            mock.Setup(x => x.GetBonus(It.IsAny<decimal>())).Returns<decimal>(total => total);
            mock.Setup(x => x.GetBonus(It.IsInRange<decimal>(200, 300, Range.Inclusive))).Returns<decimal>(total => total - 10);


            var target = new LinqValueCalculator();
            target.SetBonus(mock.Object);

            Product[] p = { 
                              new Product {Name="pilka",Description="uuu",Price=100},
                              new Product {Name="pilka 2",Description="uuu",Price=101},
                          };

            decimal sum = target.ValueProduct(p);
            Assert.AreEqual(sum, 191);

            List<Product> l = p.ToList<Product>();
            l.Add(new Product { Price = 1000 });
            sum = target.ValueProduct(l);
            Assert.AreEqual(sum, 1201);
            */
        }

        [TestMethod]
        public void ZFormat()
        {
            Decimal d = new Decimal(2.0);
            string str = d.ToString("F2");
            Assert.AreEqual("2,00", str);
        }
    }
}
